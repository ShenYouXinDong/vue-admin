import Cookies from 'js-cookie'

const token = 'TOKEN'

export function getToken () {
  return Cookies.get(token)
}

export function setTokenKey (value) {
  return Cookies.set(token, value)
}

export function removeToken () {
  return Cookies.remove(token)
}
