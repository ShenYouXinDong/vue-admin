
const USER = 'user'
const MENU = 'menu'

export function setUser (userEntity = {}) {
  // 存储值：将对象转换为Json字符串
  // console.log('set user:')
  console.log(userEntity)
  return sessionStorage.setItem(USER, JSON.stringify(userEntity))
}

export function getUser () {
// 取值时：把获取到的Json字符串转换回对象
  let store = sessionStorage.getItem(USER) || '{}'
  // console.log(JSON.parse(store))
  return JSON.parse(store)
}

export function setMenu (menuEntity = []) {
  // 存储值：将对象转换为Json字符串
  return sessionStorage.setItem(MENU, JSON.stringify(menuEntity))
}

export function getMenu () {
// 取值时：把获取到的Json字符串转换回对象
  let store = sessionStorage.getItem(MENU) || '[]'
  return JSON.parse(store)
}
