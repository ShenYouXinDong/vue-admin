import axios from 'axios' // 引用axios
import {Promise} from 'es6-promise' // 引入Promise

// axios 配置和拦截器都不用了，这里我使用了一个动态配置数据请求地址，在App.vue中，代码在下面，这个也不是必须的。

// ^_^下面都设置一个默认的头部，使用的时候可以传入数据覆盖^_^,例如使用fetch(GET)方法时，没有请求数据，但是请求头有变化，则应写成 fetch("地址", {}, {"这里写头部的内容"})   记住没数据用一个空对象占位置
/**
 * fetch 请求方法
 * @param url
 * @param params
 * @returns {Promise}
 */
export function fetch (url, params = {}, headers = {
  'Content-Type': 'application/json', // 设置跨域头部
  'Authorization': 'JWT ' + sessionStorage.getItem('authToken')
}) {
  return new Promise((resolve, reject) => {
    axios.get(url, {
      params: params,
      headers: headers
    })
      .then(response => {
        resolve(response.data)
      })
      .catch(err => {
        reject(err.response)
      })
  })
}

/**
 * post 请求方法
 * @param url
 * @param data
 * @returns {Promise}
 */
export function post (url, data = {}, config = {
  'headers': {
    'Content-Type': 'application/json', // 设置跨域头部
    'Authorization': 'JWT ' + sessionStorage.getItem('authToken')
  }
}) {
  return new Promise((resolve, reject) => {
    axios.post(url, data, config)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err.response)
      })
  })
}

/**
 * patch 方法封装
 * @param url
 * @param data
 * @returns {Promise}
 */
export function patch (url, data = {}, config = {
  'headers': {
    'Content-Type': 'application/json', // 设置跨域头部
    'Authorization': 'JWT ' + sessionStorage.getItem('authToken')
  }
}) {
  return new Promise((resolve, reject) => {
    axios.patch(url, data, config)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err.response)
      })
  })
}

/**
 * put 方法封装
 * @param url
 * @param data
 * @returns {Promise}
 */
export function put (url, data = {}, config = {
  'headers': {
    'Content-Type': 'application/json', // 设置跨域头部
    'Authorization': 'JWT ' + sessionStorage.getItem('authToken')
  }
}) {
  return new Promise((resolve, reject) => {
    axios.put(url, data, config)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err.response)
      })
  })
}
// 下面是使用例子
// <template>
// <div id="app">
//   <router-view/>
//   </div>
//   </template>
//
//   <script>
// import axios from 'axios';
// let protocol = window.location.protocol; //协议
// let host = window.location.host; //主机
// let reg = /^localhost+/;
// if(reg.test(host)) {
//   //若本地项目调试使用
//   axios.defaults.baseURL = 'http://10.0.xx.xxx:xxxx/api/';
// } else {
//   //动态请求地址
//   axios.defaults.baseURL = protocol + "//" + host + "/api/";
// }
// axios.defaults.timeout = 30000;
// export default {
//   name: 'app',
//   axios   //这里记得导出，若请求地址永久固定一个，则就按照`普通版`配置一个baserURL就可以了
// }
// </script>
//
// <style lang="scss">  //这里我使用的是scss
//   @import '~@/style/style'
// </style>

// 由于没有使用拦截器，所以下面封装的函数在返回错误的时候需要写成err.response.data来获取返回的数据，
// 但我写的是err.response，因为这样可以拿到(status)状态码等信息，
// 若不需要判断返回的状态码，则改为err.response.data
