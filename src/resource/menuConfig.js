//
import lazyLoading from '@/utils/lazyLoading'
import Login from '@/pages/login'
import NotFound from '@/pages/404'
import Home from '@/pages/home/Home'
import BaseTable from '@/pages/demo/table/BaseTable'
import First from '@/pages/first/First'
import User from '@/pages/upms/user'
import Role from '@/pages/upms/role'
import Menu from '@/pages/upms/menu'
import Parent from '@/pages/demo/slot/Parent'
import FontAwesome from '@/pages/demo/font-awesome'
// import RegExp from '@/pages/demo/regexp'
let routes = [
  {
    path: '/', // 默认路由
    hidden: true,
    redirect: '/demo' // 重定向
  },
  // {
  //   path: '/first',
  //   component: First,
  //   name: '首页',
  //   iconCls: 'fa fa-cog fa-fw',
  //   hidden: true
  // },
  {
    path: '/login',
    component: Login,
    name: 'login',
    iconCls: 'fa fa-pencil fa-fw',
    hidden: true
  },
  {
    path: '/securityLogin',
    component: lazyLoading('security_login', true),
    name: 'oauth2_login',
    iconCls: 'fa fa-pencil fa-fw',
    hidden: true
  },
  {
    path: '/404',
    component: NotFound,
    name: '404',
    iconCls: 'fa fa-pencil fa-fw',
    hidden: true
  },
  {
    path: '/demo',
    name: '案例',
    iconCls: 'fa fa-cog fa-fw', // 图标样式class
    component: Home,
    hidden: false,
    security: '',
    children: [
      { path: 'table', component: BaseTable, hidden: false, name: '表格', iconCls: 'fa fa-table fa-fw' },
      { path: 'slot', component: Parent, name: 'slot', hidden: false, iconCls: 'fa fa-leaf fa-fw' },
      { path: 'fontAwesome', component: FontAwesome, hidden: false, name: 'font-awesome', iconCls: 'fa fa-font-awesome fa-fw' },
      { path: 'home', component: First, name: '主页', hidden: false, iconCls: 'fa fa-home fa-fw' },
      { path: 'regexp', component: lazyLoading('demo/regexp', true), hidden: false, name: '正则表达式', iconCls: 'fa fa-registered fa-fw' },
      { path: 'tree', component: lazyLoading('demo/tree', true), hidden: false, name: '树', iconCls: 'fa fa-tree fa-fw' },
      { path: 'creator', component: lazyLoading('demo/creator-code', true), hidden: false, name: '代码生成器', iconCls: 'fa fa-tree fa-fw' },
      { path: 'form', component: lazyLoading('demo/form', true), hidden: false, name: '表单', iconCls: 'fa fa-tree fa-fw' },
      { path: 'sysMenu', component: lazyLoading('demo/sysMenu', true), hidden: false, name: '测试菜单', iconCls: 'fa fa-tree fa-fw' },
      { path: 'BL', component: lazyLoading('demo/BL', true), hidden: false, name: '报表测试', iconCls: 'fa fa-tree fa-fw' },
      { path: '权限菜单测试', component: lazyLoading('demo/tPermission', true), hidden: false, name: '权限', iconCls: 'fa fa-tree fa-fw' },
      { path: '角色', component: lazyLoading('demo/role', true), hidden: false, name: '角色', iconCls: 'fa fa-tree fa-fw' }
    ]
  },
  {
    path: '/security',
    name: 'oauth2',
    iconCls: 'fa fa-wrench fa-fw', // 图标样式class
    component: Home,
    hidden: false,
    security: 'back:user:query',
    children: [
      { path: 'appUser', component: lazyLoading('security/appUser', true), hidden: true, name: 'oauth2用户', iconCls: 'fa fa-user-o fa-fw', meta: {security: 'back:user:page'} },
      { path: 'sysPermission', component: lazyLoading('security/sysPermission', true), name: 'oauth2权限', hidden: true, iconCls: 'fa fa-list fa-fw', meta: {security: 'back:permission:query'} },
      { path: 'sysRole', component: lazyLoading('security/sysRole', true), hidden: true, name: 'oauth2角色', iconCls: 'fa fa-wrench fa-fw', meta: {security: 'back:role:query'} },
      { path: 'userCredentials', component: lazyLoading('security/userCredentials', true), name: '用户凭证', hidden: true, iconCls: 'fa fa-home fa-fw' },
      { path: 'wechat', component: lazyLoading('security/tWechat', true), hidden: true, name: '微信', iconCls: 'fa fa-registered fa-fw' }
    ]
  },
  {
    path: '/upms',
    name: '权限管理',
    iconCls: 'fa fa-wrench fa-fw', // 图标样式class user-o
    component: Home,
    hidden: true,
    children: [
      { path: '/upms/user', component: User, name: '用户', hidden: true, iconCls: 'fa fa-user-o fa-fw' },
      { path: '/upms/role', component: Role, name: '角色', hidden: false, iconCls: 'fa fa-wrench fa-fw' },
      { path: '/upms/menu', component: Menu, name: '菜单', hidden: false, iconCls: 'fa fa-list fa-fw' }
    ]
  },
  {
    path: '*',
    hidden: true,
    redirect: { path: '/404' }
  }
]

export default routes
