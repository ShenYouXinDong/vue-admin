// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
import moment from 'moment'
import 'font-awesome/css/font-awesome.css'
import '@/style/base.css'
Vue.config.productionTip = false
Vue.use(ElementUI)
/* eslint-disable no-new */

// 定义过滤器
Vue.filter('dateString', function (value, format = 'YYYY-MM-DD HH:mm:ss') {
  // http://momentjs.cn/docs/#/parsing/  查询这个Year, month, and day tokens可以获得格式化例子
  return moment(value).isValid() ? moment(value).format(format) : ''
})

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  store,
  router
})
