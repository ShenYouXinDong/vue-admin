1.添加对话框配置
      addColumns: [
        {
          prop: 'name',
          label: '菜单名',
          dataType: 'varchar'
        },
        {
          prop: 'url',
          label: '路径',
          dataType: 'varchar'
        },
        {
          prop: 'type',
          label: '类型',
          dataType: 'select',
          options: [{
            value: '1',
            label: '菜单'
          }, {
            value: '0',
            label: '权限'
          }]
        }
      ]

2.如查需要校验,那么还要配置校验规则,自定义规则需要自定义校验方法
      addColumnsRule: {
        name: [
          { required: true, message: '请输入菜单名称', trigger: 'blur' },
          // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' },
          {validator: this.validateName, trigger: 'blur'}
        ],
        url: [
          { required: true, message: '请输入url', trigger: 'blur' },
          // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' },
          {validator: this.validateUrl, trigger: 'blur'}
        ]
      }

    校验方法
        validateName (rule, value, callback) {
          // 只能存在中文与字母
          let pattern = /^[\u4e00-\u9fa5A-Za-z]{4,16}$/
          if (pattern.test(value)) {
            callback()
          } else {
            callback(new Error('请输入[2-16]字母中文!'))
          }
        },
        validateUrl (rule, value, callback) {
          // 只能存在中文与字母
          let pattern = /^[A-Za-z/]{4,16}$/
          if (pattern.test(value)) {
            callback()
          } else {
            callback(new Error('请输入[4-16]字母包括(/)!'))
          }
        }



      columns: [],
      applicationName: '',
      controller: '',
      customAddColumnsData: {},
      addColumns: [
        {
          prop: 'name',
          label: '菜单名',
          dataType: 'varchar'
        },
        {
          prop: 'url',
          label: '路径',
          dataType: 'varchar'
        },
        {
          prop: 'type',
          label: '类型',
          dataType: 'select',
          options: [{
            value: '1',
            label: '菜单'
          }, {
            value: '0',
            label: '权限'
          }]
        }
      ],
      addColumnsRule: {
        name: [
          { required: true, message: '请输入菜单名称', trigger: 'blur' },
          // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' },
          {validator: this.validateName, trigger: 'blur'}
        ],
        url: [
          { required: true, message: '请输入url', trigger: 'blur' },
          // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' },
          {validator: this.validateUrl, trigger: 'blur'}
        ]
      },
      addData: {},
      customEditColumnsData: {},
      editColumns: [
        {
          prop: 'name',
          label: '菜单名',
          dataType: 'varchar'
        },
        {
          prop: 'url',
          label: '路径',
          dataType: 'varchar'
        },
        {
          prop: 'type',
          label: '类型',
          dataType: 'select',
          options: [{
            value: '1',
            label: '菜单'
          }, {
            value: '0',
            label: '权限'
          }]
        }
      ],
      editColumnsRule: {},
      editType: 'edit',
      editData: {},
      customSearchColumnsData: {},
      searchColumns: [
        {
          prop: 'name',
          label: '菜单名',
          dataType: 'varchar'
        },
        {
          prop: 'createDateTime',
          label: '创建时间',
          dataType: 'datePicker'
        }
      ],
      searchColumnsRule: {},
      searchType: 'search',
      multipleSelection: [],
      editDialogVisible: false,
      addDialogVisible: false
