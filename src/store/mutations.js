export default {
  setNavMenuSpan (state, {span}) {
    state.navMenuSpan = span
  },
  setCollapse (state, {isCollapse}) {
    state.isCollapse = isCollapse
  },
  setUserId (state, {userId}) {
    state.userId = userId
  },
  setUserName (state, {username}) {
    state.username = username
  },
  setToken (state, {token}) {
    state.token = token
  },
  setRoles (state, {roles}) {
    state.roles = roles
  },
  setResources (state, {resources}) {
    state.resources = resources
  }
}
