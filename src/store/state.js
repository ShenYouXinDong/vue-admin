// 状态模块
export default {
  username: '', // 用户名
  userId: '', // 用户id
  token: '', // token
  roles: [], // 用户对应的数组
  resources: [], // 用户对应的资源(即权限)
  navMenuSpan: 4, // 菜单模块大小
  isCollapse: false // 菜单是否折叠
}
