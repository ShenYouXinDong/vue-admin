export default {
  setNavMenuSpan ({commit}, span) {
    // 提交mutation:更新状态
    commit('setNavMenuSpan', {span})
  },
  setCollapse ({commit}, isCollapse) {
    commit('setCollapse', {isCollapse: isCollapse})
  },
  setUserId ({commit}, userId) {
    commit('setUserId', {userId: userId})
  },
  setUserName ({commit}, username) {
    commit('setUserName', {username: username})
  },
  setToken ({commit}, token) {
    commit('setToken', {token: token})
  },
  setRoles ({commit}, roles) {
    commit('setRoles', {roles: roles})
  },
  setResources ({commit}, resources) {
    commit('setResources', {resources: resources})
  }
}
