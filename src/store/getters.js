import {getUser} from '@/utils/session'
// 包含n个基于state的getter计算属性方法的对象模块
export default {
  navMenuSpan (state) { // 当读取属性值时自动调用并返回属性值
    return state.isCollapse ? 2 : 4
  },
  username (state) {
    let user = getUser()
    if (user.name === undefined) {
      return state.username === '' ? user.username : state.username
    }
    return state.username === '' ? getUser().name : state.username
  },
  userId (state) {
    return state.userId === '' ? getUser().id : state.userId
  },
  token (state) {
    let user = getUser()
    if (user.token === undefined) {
      return state.token === '' || state.token === undefined ? user.access_token : state.token
    }
    return state.token === '' || state.token === undefined ? user.token : state.token
  },
  roles (state) {
    let user = getUser()
    if (user.roleList === undefined) {
      return (state.roles === '' || state.roles === undefined) ? user.sysRoles : state.roles
    }
    return (state.roles === '' || state.roles === undefined) ? user.roleList : state.roles
  },
  resources (state) {
    let user = getUser()
    console.log('resource赋值:')
    console.log(user)
    if (user.resources === undefined || user.resources === null || user.resources.length === 0) {
      let resources = state.resources.length === 0 ? user.permissions : state.resources
      console.log(resources)
      return resources
    }
    return (state.resources === '' || state.resources === undefined || state.resources.length === 0) ? user.menuList : state.resources
  }
}
