## vue-admin

> 基于饿了嘛element编写项目工程,由代码生成器生成相关基本代码,其它的代码基于根据数据驱动视图的思想,进行配置 

### Build Setup

``` bash
# 安装依赖
npm install or
yarn

# 服务端地址 localhost:8080
npm run dev or
yarn start

# 最小体积构建
npm run build

# 构建工程且可看到分析日志
npm run build --report
```

详细请看示例代码: [demo](https://gitee.com/ShenYouXinDong/vue-admin/blob/master/src/pages/demo/sysMenu/index.vue)

### 配置详细步骤
markdown basic [markdown](https://daringfireball.net/projects/markdown/basics)
* 1.代码直接生成
  + 1.1有分页换行,以及增,修,改,删除
    ![Alt first](/images/first.jpg)
* 2.配置添加对话框
    <pre> 代码展示
    /// 1.配置对话框中列内容
    addColumns: [
            {
              prop: 'name',
              label: '菜单名',
              dataType: 'varchar'
            },
            {
              prop: 'url',
              label: 'url',
              dataType: 'varchar'
            },
            {
              prop: 'type',
              label: '菜单类型',
              dataType: 'select',
              options: [
                {
                  value: 0,
                  label: '权限'
                },
                {
                  value: 1,
                  label: '菜单'
                }
              ]
            }
          ]
       /// 2.配置增加对话框中的校验规则
       addColumnsRule: {
               name: [
                 {required: true, message: '请输入菜单名', trigger: 'blur'}, /// 这个是element自带配置的
                 {validator: this.validateName, trigger: 'blur'} /// 自定义校验规则
               ]
             }
        /// 下面是自定义校验规则函数
        validateName (rule, value, callback) {
              // 只能存在中文与字母
              let pattern = /^[\u4e00-\u9fa5A-Za-z]{4,16}$/
              if (pattern.test(value)) {
                callback()
              } else {
                callback(new Error('请输入[4-16]字母中文!'))
              }
            }
    </pre>
    ![Alt second](/images/second.jpg)
    + 2.1 对话框的详细配置信息
    ![Alt third](/images/third.jpg)
    + 2.2 如果要做数据校验,要做addColumnsRule配置
    ![Alt four](/images/four.jpg)
    ![Alt five](/images/five.jpg)
* 3.删除操作代码生成的时候功能就ok
* 4.编辑与添加对话框一样的配置
* 5.模糊查询
  + 5.1 数据配置与添加对话框一致:例如
     <p>vue代码</p>
    
     <pre><code>
     /// 模糊查询配置列
       searchColumns: [
         {
           prop: 'name',
           label: '菜单名',
           dataType: 'varchar'
         },
         {
           prop: 'datetimeValue',
           label: '时间范围',
           dataType: 'datePicker'
         }
       ]
       
       /// 获取数据(数据格式化如:时间)
       getSearchFormData (formData = {}, type, refs) {
             console.log(formData)
             console.log(type)
             /// 若formData数据为空,那么直接返回
             if (JSON.stringify(formData) === '{}') {
               return
             }
             /// searchForm做整体校验,通过提交
             refs['searchForm'].validate((valid) => {
               if (valid) {
                 /// 提交
                 let page = {
                   limit: 5,
                   page: 1
                 }
                 /// 时间格式化
                 formData.createDateTimeStart = moment(formData.datetimeValue[0]).isValid() ? moment(formData.datetimeValue[0]).format('YYYY-MM-DD HH:mm:ss') : undefined
                 formData.createDatetimeEnd = moment(formData.datetimeValue[1]).isValid() ? moment(formData.datetimeValue[1]).format('YYYY-MM-DD HH:mm:ss') : undefined
                 /// 删除对象中指定元素(一般时间值是数组)
                 delete formData.datetimeValue
                 /// 将formData对象赋给page对象
                 Object.assign(page, formData)
                 /// 模糊查询提交
                 this.fetch(page)
               }
             })
           }
     </code></pre>
    
